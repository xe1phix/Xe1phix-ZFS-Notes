#!/bin/sh


zpool import -o readonly=on

dd if=/dev/random of=/mypool/usr/ports/somefile bs=1m count=10


## create a snapshot to preserve this version of the filesystem. If you specify the -r (recursive) flag,
zfs snapshot -r mypool/usr/ports@firstsnapshot




## ZFS has the "rollback" operation, which reverts all changes written since the snapshot
zfs rollback -r mypool/usr/ports@firstsnapshot
zfs list -t all -o name,used,refer,written -r mypool





## Sending and Receiving Snapshots
zfs snapshot mypool/myfiles@backup
zfs send mypool/myfiles@backup > /mnt/filesystem-backup

## restore the backup from the file
zfs receive -v mypool/myfiles < /mnt/filesystem-backup


## copy that snapshot to a remote server
zfs send mypool/myfiles@backup | ssh you@remoteserver zfs receive -v otherpool/myfiles


## To grow a mirror, expansion will need to be set on the pool.
zpool set autoexpand=on mypool



zfs set checksum=sha256 mypool



zfs get compressratio mypool

zfs set checksum=sha256,verify mypool/vms

zfs set dedup=verify mypool/vms



##-===========================-##
##   [+] ARC, L2ARC and ZIL
##-===========================-##


##-==========================================-##
##   [+] ARC (Adaptive Replacement Cache)
##-==========================================-##

## When your data becomes larger than the amount of memory you have, 
## however, it spills over into the disk and operations become much slower. 
## You can use a fast storage device like an SSD for a "level 2" ARC, or L2ARC. 
## The L2ARC is a caching "layer" between the RAM (very fast) and the disks (not so fast). 
## To add an L2ARC to your existing zpool, we might do:


zpool add mypool cache /tutorial/ssd



##-===============================-##
##   [+] ZIL (ZFS Intent Log) 
##-===============================-##

## ZIL - is the write cache writes the file metadata to a 
## faster device to increase the write throughput. 
## A ZIL basically turns synchronous writes into asynchronous writes, 
## improving overall performance. 

zpool add mypool log /tutorial/file7


## add mirrored ZILs for even more protection.
zpool add mypool log mirror /tutorial/file7 /tutorial/file8



L2ARC, which is the read cache








# Supported boot parameters
* rollback=\<on|yes|1\> Do a rollback of specified snapshot.
* zfs_debug=\<on|yes|1\> Debug the initrd script
* zfs_force=\<on|yes|1\> Force importing the pool. Should not be necessary.
* zfs=\<off|no|0\> Don't try to import ANY pool, mount ANY filesystem or even load the module.
* rpool=\<pool\> Use this pool for root pool.
* bootfs=\<pool\>/\<dataset\> Use this dataset for root filesystem.
* root=\<pool\>/\<dataset\> Use this dataset for root filesystem.
* root=ZFS=\<pool\>/\<dataset\> Use this dataset for root filesystem.
* root=zfs:\<pool\>/\<dataset\> Use this dataset for root filesystem.
* root=zfs:AUTO Try to detect both pool and rootfs

In all these cases, \<dataset\> could also be \<dataset\>@\<snapshot\>.

The reason there are so many supported boot options to get the root filesystem, is that there are a lot of different ways too boot ZFS out there, and I wanted to make sure I supported them all.
the snapshot part) dataset to find _where_ it should mount the dataset. Or it will use the name of the dataset below the root filesystem (<code>rpool/ROOT/debian-1</code> in this example) for the mount point.
